"""
config file for app
"""

domains=["bapdada","localhost","127.0.0.1"]
#database = ''  #this will default to <app name>.db (no path - ie in the current working directory)
port = '9004'


urlpath=""
default_class="Page"

#permits={} #basis of permit system

guests=True #do guests have access by default?
#guest_comments=False

#mailfrom= 'admin@whatever.com' #email from address
#mailto= 'admin@whatever.com' #email to address

meta_description="BapDada avyakt murli spiritual teaching" #site description text for search engines
meta_keywords="BapDada,avyakt,murli,spiritual,teaching" #comma separated list of keywords for search engines

#page_welcome=13 #welcome age for newly validated members to arrive at


links=True 
ratings="admin"

chronological=False
show_who=False
show_when=False
show_score=False

#registration_method="admin"