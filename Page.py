"""
override class for grace.page

"""
from grace.render import html
from grace.lib import *
from grace.Page import Page as basePage

from grace.data import execute

import os, string

import urllib.request, urllib.parse, urllib.error, re

from calendar import month_name

# sentence capitalisation within a text page
#
# based on https://stackoverflow.com/questions/22800401/how-to-capitalize-the-first-letter-of-every-sentence"
#
# the following regex:
#  1. looks for a preceding full stop, question mark, or exclamation mark, OR start of line, OR hash+space (grace heading-syntax)
#  2. capitalises the next character
# thus fixing most instances of missing capitalisation

sentences = re.compile(r'(?m)(?<=[\.\?!]\s)(\w+)|(^\w+)|(?<=# )(\w+)')

def cap(match):
  return(match.group().capitalize())

def capitalise_sentences(txt):
  return sentences.sub(cap, txt)

# Page class override
class Page(basePage):

  def post(self,req):
    """fix date and format when posting a draft
       - overrides grace post()
    """
    if self.kind!='page': #safety valve
      return basePage.post(self,req)
    if basePage._posted(self,req):  
      self.fix_bapdada_date(req)
#      self.reform_text(req)
#      if not req.warning:
#        req.warning='comments reformed'
      self.flush()
    return self.view(req)
  post.permit='create page'

  def latest(self, req):    
    "override grace version"
    req.pages=self._latest(req,order="uid desc")
    req.title="additions"
    req.prep="to"
    req.page='latest' # for paging
    return self.listing(req)

  def favourites(self,req):
    "top rated pages, in date order"
    lim=page(req,50)
    req.pages=self.list(kind="page",stage="posted",rating=3,orderby="`when`",limit=lim)
    req.title="favourites"
    req.prep="from"
    req.page='favourites' # for paging
    return self.listing(req)
    

  def get_navbar_links(self):
    "override grace version"
    home=self.get(1)
    return (
     ("home",home.url(),"home"),
     ("favourites",home.url("favourites"),"deepian's favourite murlis"),
     ("additions",home.url("latest"),"latest additions"),
     ("at twitter","http://twitter.com/bapdadainfo","@bapdadainfo"),
    )

################# yearly listings ##################

  def get_child_pages(self, req, limit=50, descend=False):
    "force viewing of descendents for yearly listings"
    if req.year:
      descend=True
    return basePage.get_child_pages(self, req=req, limit=limit, descend=descend)


################# publishing (test) ####################

  def publish(self,req):
    "publish site to flat files"
    pass



################# temporary import/fix routines for setting up text versioning #####################

  from grace.data import execute

  @classmethod
  def entrail(self,req):
    "temporary import process to incorporate old data in trail"
    if req.user.uid!=2:
      return req.user.error(req,"not authorised")
    all=self.list()
    xsource=""
    for source in (
"110215",
"110627",
"110805",
"120210",
"120316",
"120418",
"120522",
"120526",
"120622",
"120701",
"120709",
"120721",
"120727",
"120809",
"120822",
):
      print("processing bapdada"+source)
      c=0
      for i in all:
        res=execute("select name,text from bapdada%s.pages where uid=%s" % (source,i.uid))
        if res:
          if xsource:
            xres=execute("select name,text from bapdada%s.pages where uid=%s" % (xsource,i.uid))
          else:
            xres=[] 
          if xres:
            xn=str(xres[0]["name"])
            xt=str(xres[0]["text"])
          else:
            xn=xt=""
          n=str(res[0]["name"])
          t=str(res[0]["text"])
          if (n!=xn) or (t!=xt):
            text="%s\n##\n\n%s" % (n,t)
            self.Text.create(page=i.uid,text=text,when=int("20"+source))
            c+=1
      print('%s texts added' % c)
      xsource=source
    return "DONE"

############################### utilities ###############################

  _fifties=(764,) # children of this require special date handling

#  comment_rule=re.compile(r'(\()(.*?)(\))')
#
#  def reform_text(self,req,bolden=False):
#    "fix italics and bold to be consistent with trance messages"
#  
#    def italicise(match):
#      ""
#      source=match.groups()[1].strip()
#      return " ~ ("+source+") ~ "
  
#    if self.kind=='episode':
#      if (self.name.find("trance")>0):
#        req.warning="trance message - reform skipped"
#      elif (bolden and (self.text.find("^")>=0)) or (self.text.find(" ~ (")>=0):
#        req.warning="reformed already - skipped"
#      else:
#        if bolden:
#          self.text=self.text.replace("~","^")  #make italic words bold
#        self.text=self.comment_rule.sub(italicise,self.text)
#        self.flush() #store changes
#  reform_text.permit='dummy' 

#  def reform(self,req):
#    "make text between ( ) italic"
#    self.reform_text(req,bolden=False)
#    req.message="comments reformed"
#    return self.view(req) 
#  reform.permit='admin page' 

#  def reform_all(self,req):
#    "fix italics and bold etc for every episode"
#    murlis=self.list(kind='episode',where='(name like "avyakt bapdada %") or (name like "sakar bapdada %")')
#    c=0
#    s=0
#    for i in murlis:
#      i.reform_text(req)
#      if req.warning:
#        s+=1 
#        req.warning=''
#      else:
#        c+=1
#    req.message=" %s reformed, %s skipped" % (c,s)     
#    return self.get(1).view(req) 
#  reform_all.permit='admin page' 

  def fix_bapdada_date(self,req):
    ""
    try:
     if self.parent in self._fifties:
      date='1/1/1950'    
     elif "revision" in self.name:
      date='1/1/1960'    
     else:
      title,day,month,year=self.name.rsplit(' ',3)
#      print i.uid,':',day,' - ',month,' - ',year
      if day[1] in ['0','1','2','3','4','5','6','7','8','9']:
        day=day[:2]
      else:
        day=day[0]	 
      month=list(month_name).index(month)
      date='%s/%s/%s' % (day,month,year)
     self.when=DATE(date)
#     print "WHEN=>",self.when
     self.set_seq()
     self.flush()
    except:
#     raise
     req.error="date handling problem - date not set"
 
  def fix_bapdada_dates(self,req):
    ""
    murlis=self.list(kind='episode',where='(name like "avyakt bapdada %") or (name like "sakar bapdada %")')
    for i in murlis:
     i.fix_bapdada_date(req) 
    req.message="BapDada dates updated"
    return self.get(1).view(req)
  fix_bapdada_dates.permit='admin page'
  fix_bapdada_dates=classmethod(fix_bapdada_dates)


  # tag utilities ###############

  def minor_tags(self):
    "returns a list of tags used less than 5 times" 
    db="bapdada"
    res=self.execute(f"select name, count(page) as hits from {db}.tags group by name order by name")
 #   return str(res)
    tags=[]
    for tag in res:
      if tag["hits"]<5:
        tags.append(tag)
    return tags

  def list_minor_tags(self,req):
    "a utility function to LIST tags used less than 5 times"
    tagnames=self.minor_tags()
    show=f"{len(tagnames)} minor tag names from total of {self.tagname_count()} tag names:<br/><br/>"
    for tag in tagnames:
        show+=f'{tag["name"]} : {tag["hits"]}<br/>'
    return show

  def remove_minor_tags(self,req):
    "a utility function to DELETE tags used less than 5 times"
    tagnames= [tag["name"] for tag in self.minor_tags()]
    db="bapdada"
    if tagnames:
      command = f"delete from {db}.tags where name in {tuple(tagnames)}"
      self.execute(command)
      return f"{len(tagnames)} tag names REMOVED: </br></br> {command}"
    return "no minor tags found"


  # November 2024 bapdada text fixes
  #
  # These should be suitable for application to EVERY text page in bapdada DB
  #
  # first test extensively on a page by page basis:



  def fix(self,req):
    "sundry text replacements"
    t=self.text
    t=t.replace("supersensuous","super-sensuous")
    t=t.replace("body conscious","body-conscious")
    t=t.replace("soul conscious","soul-conscious")
    t=t.replace("double foreign","double-foreign")
    t=t.replace("Double foreign","Double-foreign")
    t=t.replace("heart throne","heart-throne")
    t=t.replace("heart to heart","heart-to-heart")
    t=t.replace("self sovereign","self-sovereign")
    t=t.replace("self realisation","self-realisation")
    t=t.replace("ever ready","ever-ready")
    t=t.replace("multi-million","multimillion")
    t=t.replace("double light","double-light")
    t=t.replace("'ll"," will")
    t=t.replace("'ve"," have")
    # the following are assorted un-capitalisations, but are more elegantly done by replace()
    t=t.replace("One Father","one Father")
    t=t.replace("Highest on High Father","highest on high Father")
    t=t.replace("Highest Father","highest Father")
    t=t.replace("Spiritual Father","spiritual Father")
    t=t.replace("True Father","true Father")
    t=t.replace("Unlimited Father","unlimited Father")
    t=t.replace("Eternal Father","eternal Father")
    # some items from fix2 below can be dealt with here
    t=t.replace("Golden Age","golden age")
    t=t.replace("Silver Age","silver age")
    t=t.replace("Copper Age","copper age")
    t=t.replace("Iron Age","iron age")
    t=t.replace("Confluence Age","confluence age")
    t=t.replace("Diamond Age","diamond age")
    t=t.replace("Golden and","golden and")
    t=t.replace("Golden age","golden age")
    t=t.replace("Silver","silver")
    t=t.replace("Copper and","copper and")
    t=t.replace("Copper age","copper age")
    t=t.replace("Iron age","iron age")
    t=t.replace("New Year","new year")
    t=t.replace("New Age","new age")
    # deal with the small ones: add a space so to avoid mismatch with longer words (we will miss any with no trailing space, e.g. ending a sentence, but hey ho!)
    for w in ("The ","He ","Him ","Him.","His ","She ","Her ","Of ","On "):
      t=t.replace(w, w.lower())
    # capitalise all sentences to fix whatever we have wrongly lowercased 
    t=capitalise_sentences(t)
    # fix BapDada instances wrongly uncapitalised by the previous instruction 
    t=t.replace("Bapdada","BapDada")
    # save it and show it
    self.text=t
    self.flush()
    if not req.message:
      req.message="fix applied"
    return self.view(req)

  def deb(self,req):
    "clean and de-bold and fix self.text, implying <small>..</small> at paragraph level"
    # safety first: minimise the chance of accidentally destroying existing legitimate bolding of words
    if (self.when<=DATE("31/12/1989")) or (self.parent!=36):
      req.warning="not allowed: nothing done"
      return self.view(req)

    # paragraph level fixes before general debolding
    paras=self.text.splitlines()
    newparas=[]
    for p in paras:
      # imply <small> for unbolded paragraphs
      x=p.strip()
      if x and not (x[0] in "*[<>#-("):
        p=f"<small>\n{x}\n</small>"
 #       print(p)
      # remove what would be leading and trailing spaces after debolding
      p=p.strip(' *')
      newparas.append(p)
    self.text='\n'.join(newparas)

    # debold
    self.text=self.text.replace("**","")

    # fix it, save it, and show it
    req.message="text de-bolded and fixed"
    return self.fix(req)

